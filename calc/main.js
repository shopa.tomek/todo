// @ts-nocheck
const currentNumber = document.querySelector('.currentNr');
const previousNumber = document.querySelector('.prevNr p');
const mathSign = document.querySelector('.mathSign');
const numberButtons = document.querySelectorAll('.number');
const operators = document.querySelectorAll('.operator');
const equal = document.querySelector('.equals');
const clear = document.querySelector('.clear');
const historyList = document.querySelector('.historyList');
const historyBtn = document.querySelector('.historyBtn');

// check //
// equal.addEventListener('click', ()=>{
//   alert("clicked");
// })

let result = '';

function displayNumbers(){
  if(this.textContent === '.' && currentNumber.innerHTML.includes('.')) return;
  if(this.textContent === '.' && currentNumber.innerHTML === '') return currentNumber.innerHTML = '.0'

  currentNumber.innerHTML += this.textContent; 
}

function operate() {
  if(currentNumber.innerHTML === '' && this.textContent === '-'){
    currentNumber.innerHTML = '-';
    return;
  }

  else if(currentNumber.innerHTML === ''){
    return;
  }

  if(mathSign.innerHTML !== '') {
    showResult();
  }
  previousNumber.innerHTML = currentNumber.innerHTML;
  mathSign.innerHTML = this.textContent;
  currentNumber.innerHTML = '';
}

function showResult() {
  if(previousNumber.innerHTML === '' || currentNumber.innerHTML === '') return;
  let a = Number(currentNumber.innerHTML);
  let b = Number(previousNumber.innerHTML);
  let operator = mathSign.innerHTML;
  
  switch(operator) {
    case '+' :
    result = a + b;
    break;
    case '-' :
    result = b - a;
    break;
    case 'x' :
    result = a * b;
    break;
    case ':' :
    result = b / a;
    break;
    case '2^' :
    result = b ** a;
    break;
  }
  
  currentNumber.innerHTML = result;
  previousNumber.innerHTML = '';
  mathSign.innerHTML = '';
}




function clearScreen() {

}

function clearHistory() {

}

// nasluchiwanie przyciskow

operators.forEach((button) => button.addEventListener('click', operate));
numberButtons.forEach((button) => button.addEventListener('click', displayNumbers));
equal.addEventListener('click', showResult);
clear.addEventListener('click', clearScreen);
historyBtn.addEventListener('click', clearHistory);
